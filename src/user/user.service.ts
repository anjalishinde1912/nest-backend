import { Injectable } from '@nestjs/common';
import { UserDto } from './dto/user.dto';
import { UserEntity } from './user.entity';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(private userRepository: UserRepository) {}

  async createUser(user: UserDto): Promise<boolean> {
    return await this.userRepository.createUser(user);
  }

  async findUserByEmail(email: string): Promise<any> {
    return await this.userRepository.findUserByEmail(email);
  }
}
