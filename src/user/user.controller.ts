import {
  Body,
  Controller,
  HttpCode,
  Post,
  Get,
  UseGuards,
  Request
} from '@nestjs/common';
import { AuthService } from 'src/auth/auth.service';
import { LocalAuthGuard } from '../auth/auth.guard';
import { LoginDto } from './dto/login.dto';
import { UserDto } from './dto/user.dto';
import { UserService } from './user.service';
import { JwtGuard } from '../auth/jwt.guard';

@Controller('user')
export class UserController {
  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}

  @Post('sign-up')
  @HttpCode(200)
  async signUp(@Body() user: UserDto): Promise<boolean> {
    return this.userService.createUser(user);
  }

//  @UseGuards(LocalAuthGuard)
  @Post('login')
  async logIn(@Body() login: LoginDto, @Request() req): Promise<UserDto> {
    return this.authService.login(login);
  }

  @UseGuards(JwtGuard)
  @Get('test')
  async test(): Promise<string> {
    return 'sucess';
  }
}
