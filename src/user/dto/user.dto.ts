import { ApiProperty } from '@nestjs/swagger';

export class UserDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  emailId: string;

  @ApiProperty()
  password: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  phoneNo: string;

  @ApiProperty()
  age: number;

  @ApiProperty()
  disease: string;

  @ApiProperty()
  medicalHistory: string;

  @ApiProperty()
  address: string;

  @ApiProperty()
  gender: string;

  @ApiProperty()
  access_token: string;
}
