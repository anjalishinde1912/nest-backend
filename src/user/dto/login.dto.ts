import { Column, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

export class LoginDto {
  @ApiProperty()
  emailId: string;

  @ApiProperty()
  password: string;
}
