import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  emailId: string;

  @Column()
  password: string;

  @Column()
  name: string;

  @Column()
  phoneNo: string;

  @Column()
  age: number;

  @Column()
  disease: string;

  @Column()
  medicalHistory: string;

  @Column()
  address: string;

  @Column()
  gender: string;
}
