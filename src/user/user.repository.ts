/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDto } from './dto/user.dto';
import { UserEntity } from './user.entity';

@Injectable()
export class UserRepository {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async createUser(user: UserDto): Promise<boolean> {
    await this.userRepository.save(user);
    return true;
  }

  async findUserByEmail(emailId: string): Promise<any> {
    return await this.userRepository.findOne({ where: { emailId } });
  }
}
