import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserDto } from '../user/dto/user.dto';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.userService.findUserByEmail(username);
    if (user && user.password == pass) {
      return user;
    }
    return null;
  }

  async login(user: any): Promise<any> {
      const payload = { email: user.emailId, password: user.password };
      try {
        const token = this.jwtService.sign(payload, { secret: 'SECRET' }); // Replace 'your-secret-key' with your actual secret key
        const user = await this.userService.findUserByEmail(payload.email);
        user.access_token = token;
        return {
          user: user
        };
      } catch (error) {
        // Handle the error appropriately (e.g., logging, returning an error response)
        throw new Error('Failed to generate access token');
      }
  }
}
